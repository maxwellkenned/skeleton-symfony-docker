<?php

namespace App\Controller;

use FOS\RestBundle\Controller\{
    AbstractFOSRestController,
    Annotations as Rest
};
use Symfony\Component\HttpFoundation\{
    Request,
    Response
};

/**
 * Class HomeController
 *
 * @package App\Controller
 */
class HomeController extends AbstractFOSRestController
{
    /**
     * @Rest\Get("/", name="index")
     * @param Request $request
     *
     * @return string
     */
    public function indexAction(Request $request)
    {
        return new Response('Hello World!');
    }
}
