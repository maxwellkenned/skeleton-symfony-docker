<?php

namespace App\Tests\ControllerTest;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class HomeControllerTest
 *
 * @package App\Tests\ControllerTest
 */
class HomeControllerTest extends WebTestCase
{
    public function testindexAction()
    {
        $client =  static::createClient();

        $client->request('GET', '/api/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}