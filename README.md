Skeleton-symfony-docker
==============

[![pipeline status](https://gitlab.com/maxwellkenned/skeleton-symfony-docker/badges/master/pipeline.svg?style=flat-square)](https://gitlab.com/maxwellkenned/skeleton-symfony-docker/commits/master)
[![coverage report](https://gitlab.com/maxwellkenned/skeleton-symfony-docker/badges/master/coverage.svg?style=flat-square)](https://gitlab.com/maxwellkenned/skeleton-symfony-docker/commits/master)
<!--![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/maxwellkenned/skeleton-symfony-docker?style=flat-square)-->

Projeto back-end feito em symfony 4 para o maior ecommerce do mundo quiçá do Brasil.

# Instalação

Primeiro, crie uma pasta da raiz do computador:

```bash
sudo mkdir /projeto && cd /projetos
```

Depois, clone esse repositório:
 
```bash
git clone https://gitlab.com/maxwellkenned/skeleton-symfony-docker.git
```

Depois, execute o seguinte comando para rodar o projeto:

```bash
composer dev
```

Terminou, você pode visitar a aplicação pela seguinte URL:

#### http://localhost/api/


# Use xdebug!

### PhpStorm

> Em: File > Settings > Languages & Frameworks > PHP > Servers
>
>Crie um novo server, no campo Port coloque a porta externa do nginx:

![Criando server](https://i.ibb.co/bN2bM7S/1.png)
>
> Depois em: Run > Edit Configurations...
>
> Crie um novo PHP Remote Debug:

![Criando remote debug](https://i.ibb.co/YczcyLT/2.png)
